/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fmg.ausbildungsstart.projekt.gui;

import fmg.ausbildungsstart.projekt.gui.ControlDialog;
import fmg.ausbildungsstart.projekt.logic.AirplaneManager;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JPanel;

import static org.junit.Assert.fail;
import org.junit.Test;

/**
 *
 * @author consult
 */
public class TestGruppe2 {

    @Test
    public void testaddToHistory(){
        ControlDialog d=new ControlDialog(null);
        for(int a=0;a<100;a++){
            d.addToHistory(String.valueOf(a));
            test(d.getHistory().size()<=10,"Historie soll nicht mehr als 10 Einträge haben");
            test(d.getHistory().contains(String.valueOf(a)),"Flug wird nicht zur Historie hinzugefügt");
        }

        //test(d.getHistory().contains("Test"),"Flug wird nicht zur Historie hinzugefügt");
    }

    @Test
    public void testReset(){
        ControlDialog d=new ControlDialog(null);
        for(int a=0;a<100;a++){
            d.addToHistory(String.valueOf(a));
            test(d.getHistory().size()<=10,"Historie soll nicht mehr als 10 Einträge haben");
            test(d.getHistory().contains(String.valueOf(a)),"Flug wird nicht zur Historie hinzugefügt");
        }
        d.reset();
        test(d.getHistory().size()==0,"Historie wurde nicht gelöscht");
    }

    @Test
    public void testComponents(){       
        ControlDialog d=new ControlDialog(null);

        LayoutManager layout=d.getLayout();
        test(layout.getClass().getName().equals(BorderLayout.class.getName()),"Das Layout des Fensters muss ein BorderLayout sein");

        Component[] comp=d.getContentPane().getComponents();

        JPanel buttonpanel=null;JList list;
        boolean panel=false,jlist=false;
        int count=0;
        for (Component c:comp) {
            count++;
            String t=c.getClass().getName();


            if(t==JPanel.class.getName()){
                panel=true;
                buttonpanel=(JPanel)c;
            }
            else if(t==JList.class.getName()){
                list=(JList) c;
                jlist=true;
            }
        }

        if(count!=2){
            if(panel&&jlist){
                fail("Zu viele Componenten hinzugefügt");
            }
            test(panel,"Es fehlt ein Panel das die Buttons beinhaltet");
            test(jlist,"Es fehlt eine JList für die Historie");
        }

        LayoutManager buttonlayout=buttonpanel.getLayout();
        String layoutclass=buttonlayout.getClass().getName();
        test(layoutclass.equals(GridLayout.class.getName()),"Das Layout des Panels muss ein GridLayout sein");

        //Buttons
        Component[] buttons=buttonpanel.getComponents();
        if(buttons.length<3){
            fail("Zu wenige Buttons. Es müssen 3 sein");
        }else if(buttons.length>3){
            fail("Zu viele Buttons. Es müssen 3 sein");
        }
        ArrayList<String> accepted=new ArrayList<>();
        accepted.add("►");
        accepted.add("||");
        accepted.add("■");

        for(int a=0;a<3;a++){
            JButton button=(JButton)buttons[a];
            test(button.getText()==accepted.get(a),"Button "+String.valueOf(a+1)+" hat nicht das richtige("+accepted.get(a)+") Symbol");
        }

    }



    public  void test(boolean totest,String msg){
        if(!totest){
            fail(msg);
        }
    }
}
