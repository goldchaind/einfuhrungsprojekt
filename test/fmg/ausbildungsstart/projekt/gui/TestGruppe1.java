/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fmg.ausbildungsstart.projekt.gui;

import fmg.ausbildungsstart.projekt.gui.GUI;
import fmg.ausbildungsstart.projekt.logic.Airplane;
import fmg.ausbildungsstart.projekt.logic.AirplaneManager;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gjurajd
 */
public class TestGruppe1 {
    
     @Test
    public void testAddAirplane(){
        GUI gui=new GUI(null);
        Airplane a=new Airplane();
        a.setStartTime(new Date(System.currentTimeMillis()));
        gui.addAirplane(a);
        test(gui.airplanes.contains(a),"Flugzeug wurde nicht zur Liste hinzugefügt");
    }

     public  void test(boolean totest,String msg){
        if(!totest){
            fail(msg);
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
