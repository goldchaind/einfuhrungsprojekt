package fmg.ausbildungsstart.projekt.logic;

import fmg.ausbildungsstart.projekt.database.DatabaseConnection;
import fmg.ausbildungsstart.projekt.gui.GUI;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

public class AirplaneManager {

    DatabaseConnection database;
    GUI ourGUI;
    BackgroundProcess backgroundProcess = null;
    Date currentTime;
    long lastRefresh;
    double timeSpeed = 10;
    public static double timeToTakeoff=35*1000;//in Millisekunden
    ArrayList<Airplane> airplanesThisHour = null;
    ArrayList<Airplane> airplanesNextHour = null;
    SimpleDateFormat guiOutputTime = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");


    /**
     * Notwendige Methoden aufrufen, um die Anwendung automatisch zu starten.
     * Als aller erstes muss die Zeit initialisiert werden (dafür gibt es eine Methode), da alle
     * Anwendungsteile mit der Zeit rechnen Anschließend müssen die GUI und die DatabaseConnection
     * initialisiert werden. Zu guter letzt muss die Anwendung gestartet werden.
     */
    public AirplaneManager(){
        // Muss das erste sein!
        refreshTime();
        ourGUI = new GUI(this);
        database = new DatabaseConnection("devnetddb", 5192, "pdevnetd", "aba_rdo", "aba_rdo#");
        startApplication();
    }

    /*
     * Methode zum Starten der Anwendung. Zuerst soll geprüft werden, ob der Hintergrundprozess bereits
     * exisitert. Wenn ja, kann die Anwendung nicht erneut gestartet werden. Sollte der Prozess noch
     * nicht laufen, soll ein neuer gestartet werden. Zum Starten soll zuerst die Zeit aktualisiert werden
     * danach sollen die Flugzeuge für diese Stunde geladen werden. Abschließend soll das Objekt
     * BackgroundProcess erstellt werden und dieses in einem neuen Thread gestartet werden.
     */
    public void startApplication(){
        if(backgroundProcess != null){
            System.out.println("Kann nicht gestartet werden, da die Anwendung bereits läuft!");
        }else{
            refreshTime();
            loadAirplanesThisHour();
            backgroundProcess = new BackgroundProcess(this);
            new Thread(backgroundProcess).start();
        }
        // Thread starten, dafür wird der erstellte BackgroundProzess übergeben:  new Thread(backgroundProcess).start();
    }

    /**
     * #################################  Sehr anspruchsvoll  #######################################
     * Methode zum Stoppen der Anwendung. Zuerst soll geprüft werden, ob ein Hintergrundprozess bereits
     * läuft, wenn nicht, kann die Anwendung auch nicht beendet werden. Wenn der Hintergrundprozess läuft,
     * soll dieser Ordnungsgemäß über den entsprechenden Funktionsaufruf gestoppt werden. Anschließend soll
     * solange gewartet werden, bis der Hintergrundprozess meldet, dass er nicht mehr läuft. Zusätzlich
     * müssen die Zuweisungen der Variablen Hintergrundprozess, der aktuellen Zeit, der Flugzeuge dieser
     * Stunde und der Flugzeuge nächster Stunde entfernt (null) werden. Sowie die GUI resettet werden.
     */
    public void stopApplication(){
        if(backgroundProcess == null){
            System.out.println("Kann nicht beendet werden, da die Anwendung nicht läuft!");
        }else{
            backgroundProcess.stop();
            while(backgroundProcess.isRunning()){
                //Nach dem aufruf von Stop wird dieser Block benötig. Danach folgen die anderen Aufgaben
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            backgroundProcess = null;
            ourGUI.reset();
            currentTime = null;
            airplanesThisHour = null;
            airplanesNextHour = null;
        }
    }


    /**
     * Methode zum Pausieren der Anwendung. Zuerst soll geprüft werden, ob ein Hintergrundprozess bereits
     * läuft, wenn nicht, kann auch nichts pausiert werden. Wenn ein Hintergrundprozess läuft, soll der
     * Prozess ordnungsgemäß gestoppt werden und die Zuweisung von der Variablen des Hintergrundprozesses
     * soll entfernt werden (null). Im Unterschied zur stopApplication muss hier die Zeit nicht entfernt
     * werden, da die Anwendung später zum gleichen Anwendungszeitpunkt weiterlaufen soll. Die Variable
     * lastRefresh soll auf -1 gesetzt werden um erkenntlicht zu machen, dass der Wert ungültig ist.
     */
    public void pauseApplication(){
        if(backgroundProcess == null){
            System.out.println("Kann nicht pausiert werden, da die Anwendung nicht läuft!");
        }else{
            backgroundProcess.stop();
            backgroundProcess = null;
            lastRefresh = -1;
        }
    }


    /**
     * Methode zum Aktualisieren der Simulations-Uhrzeit.
     * Wenn die aktuelle Zeit noch nicht existiert (also beim ersten Aufruf der Methode), soll mithilfe der
     * Klasse GregorianCalendar das Startdatum gesetzt werden. Eine weitere Variable "letzteAktualisierung"
     * soll auf die aktuelle Systemzeit gesetzt werden.
     * Wenn die aktuelle Zeit bereits existiert, soll die Zeit wie folgt berechnet werden:
     * neueZeit = aktuelleSimulationsZeit + (aktuelleZeit - letzteAktualisierung)*eingestellteGeschwindigkeit
     * Abschließend soll die aktuelleZeit aktualisiert werden.
     * Die Zeit darf nur aktualisiert werden wenn der Wert in lastRefresh größer gleich 0 ist.
     */
    public void refreshTime(){
        if(currentTime == null){
            currentTime = new GregorianCalendar(2019,8,10,6,50,0).getTime();//new Date();
            lastRefresh = System.currentTimeMillis();
        }else{
            if(lastRefresh < 0){
                lastRefresh = System.currentTimeMillis();
            }else {
                currentTime.setTime((long) (currentTime.getTime() + (System.currentTimeMillis() - lastRefresh) * timeSpeed));
                lastRefresh = System.currentTimeMillis();
            }
        }
    }

    /**
     * Soll die Aktualisierung der Flugzeuge auf der GUI anstoßen.
     * Der GUI soll lediglich die aktuelle Zeit im Datentyp long übergeben werden.
     * Ruft dafür die Methode Draw auf.
     */
    public void updateGUI(){
        ourGUI.draw(currentTime.getTime());
    }


    /**
     * Prüft, ob die Flugzeuge für die nächste Stunde bereits vorbereitet sind. Falls ja, soll
     * false zurückgegeben werden falls die Flüge noch nicht vorhanden sind soll true zurück-
     * gegeben werden.
     * @return
     */
    public boolean isRequiredToLoadNextHour() {
        if(airplanesNextHour == null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Stößt die Abfrage aller Flugzeuge aus der Datenbank an, die diese Stunde starten/landen. Dabei soll vorher geprüft werden,
     * ob bereits die Flüge für diese Stunde geladen wurden, bzw. ob die Länge der Liste kleiner als 1 ist.
     */
    void loadAirplanesThisHour() {
        if(airplanesThisHour == null || airplanesThisHour.size() < 1) {
            airplanesThisHour = database.getFlightsByHour(currentTime);
        }else{
            throw new RuntimeException("Sollte auf keinen Fall auftreten!! 506465165435100605046405665");
        }
    }

    /**
     * Stößt die Abfrage aller Flugzeuge aus der Datenbank an, die nächste Stunde starten/landen. Dabei soll vorher geprüft werden,
     * ob bereits die Flüge für nächste Stunde geladen wurden.
     */
    public void loadAirplanesNextHour() {
        if(airplanesNextHour == null) {
            airplanesNextHour = database.getFlightsNextHour(currentTime);
        }else{
            throw new RuntimeException("Sollte nicht auftreten! 614951236489136218694165198");
        }
    }

    /**
     * Speichert die für nächste Stunde geladene Flugzeuge in die Variable der aktuellen Stunde um.
     * Damit erkenntlich wird, dass die für die nächste Stunde geladenen Flugzeuge nicht mehr gültig
     * sind, muss die Liste gelöscht werden.
     * Hinweis: Das darf nur passieren, falls Flugzeuge für die nächste Stunde geladen wurden.
     */
    public void changeAirplanesOnHourChange(){
        if(airplanesNextHour == null){
        }else{
            airplanesThisHour = airplanesNextHour;
            airplanesNextHour = null;
        }
    }


    /**
     * Es soll für alle Flugzeuge geprüft werden, ob die Zeit zum Starten/Landen gekommen ist.
     * Zuerst muss geprüft werden ob die Variable eine liste mit mindestens einem Element enthält.
     * Es soll über alle Flugzeuge dieser Stunde iteriert werden und geprüft werden, ob die aktuelle
     * Zeit größer ist als die Startzeit des Flugzeug-Objekts. Diese Flugzeuge sollen
     * in die GUI hinzugefügt und anschließend aus der Liste dieser Stunde entfernt werden.
     */
    public void addNextAirplanesToGUI() {
        if(airplanesThisHour == null || airplanesThisHour.size() < 1){
            return;
        }
        long targetTime = currentTime.getTime();

        for(int i = 0; i<airplanesThisHour.size(); i++){
            Airplane airplane = airplanesThisHour.get(i);
            if(airplane.getStartTime().getTime() < targetTime){
                ourGUI.addAirplane(airplane);
                airplanesThisHour.remove(airplane);
                i--;
            }
        }
    }


    /**
     * Liefert die aktuelle Simulationszeit zurück.
     * @return
     */
    public Date getCurrentTime() {
        return currentTime;
    }

    /**
     * Liefert die aktuelle Beschleunigung der Zeit zurück.
     * @return
     */
    public double getTimeSpeed(){
        return timeSpeed;
    }


    /**
     * Die aktuelle Zeit soll mit dem "guiOutputTime" Formatter formatiert werden.
     * @return
     */
    public String getFormattedTime() {
        return guiOutputTime.format(currentTime);
    }


    /**
     * Main Methode um die Anwendung zu starten.
     * @param args
     */
    public static void main(String[] args){
        AirplaneManager test=new AirplaneManager();
    }
}
