package fmg.ausbildungsstart.projekt.logic;

import fmg.ausbildungsstart.projekt.gui.GUI;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Airplane {

    private String flightFrom, flightTo;
    private String airplaneType;
    private String flightNumber;
    private boolean landing;
    private Date startTime;
    private int runnway;
    private int movingDirection;

    /**
     * Der Konstruktor soll die übergebenen Variablen (parameter) in die entsprechenden globalen
     * Variablen umspeichern.
     */
    public Airplane(int runnway, boolean landing, String flightNumber, Date starttime, String airplaneType, String flightFrom, String flightTo) {
        this.runnway = runnway;
        this.landing = landing;
        this.flightNumber = flightNumber;
        this.startTime = starttime;
        this.airplaneType = airplaneType;
        this.flightFrom = flightFrom;
        this.flightTo = flightTo;
    }

    public Airplane() {
        //Hier muss nichts getan werden. Dieser Konstruktor bietet die Möglichkeit
        //ein Flugzeug ohne Attribute zu erstellen.
    }

    /**
     * Berechnet die x Position auf die das Flugzeug im Fenster gezeichnet werden muss, zu der aktuellen Simulationszeit
     *
     * @param currentMiliseconds Die aktuelle Simulationszeit in Milisekunden an
     */
    public int getXPosition(long currentMiliseconds) {
        double elapsedTimeInSeconds;
        double runwayLength = 1000.0;

        //Berechne die Beschleunigung mit der Formel a=2s/t² wobei s die Anzahl der Pixel der Start/Landebahn ist (1000 pixel)
        //und t die Zeit bis das Flugzeug abhebt bzw. steht. (Der Wert steht im AirplaneManager)
        double acceleration = 2000 / Math.pow(AirplaneManager.timeToTakeoff / runwayLength, 2); //Beschleunigung

        //Variable, in der die berechnete X-Position zwischengespeichert wird.
        int panelXpos = 0;

        if (!landing) {
            //Die verstrichene Zeit soll mit der Differenz zwischen aktueller Zeit und Startzeit in Sekunden sein.
            elapsedTimeInSeconds = (currentMiliseconds - getStartTime().getTime()) / 1000.0d;

            if (movingDirection == 1) {
                panelXpos = (int) (acceleration / 2 * Math.pow(elapsedTimeInSeconds, 2));
            } else {
                panelXpos = (int) runwayLength - (int) (acceleration / 2 * Math.pow(elapsedTimeInSeconds, 2));
            }
        } else {
            //Die verstrichene Zeit soll die verbleibende Zeit sein, bis wann das Flugzeug komplett gestartet/gelandet ist
            elapsedTimeInSeconds = (AirplaneManager.timeToTakeoff - (currentMiliseconds - getStartTime().getTime())) / 1000.0d;
            if (elapsedTimeInSeconds > 0) {
                if (movingDirection == 1) {
                    panelXpos = (int) runwayLength - (int) (acceleration / 2 * Math.pow(elapsedTimeInSeconds, 2));
                } else {
                    panelXpos = (int) (acceleration / 2 * Math.pow(elapsedTimeInSeconds, 2));
                }
            } else {
                panelXpos = -1000;
            }
        }
        return panelXpos;
    }

    /**
     * Die toString() Methode liefert zu einem Objekt einen Text. Diesen Text können wir nach belieben
     * steuern. Bitte bearbeitet die Methode so, dass folgendes Stringformat zurückgeliefert wird:
     * [FlugVon - FlugNach] Abflugsdatum Uhrzeit Flugnummer Flugzeugtyp
     *
     * @return
     */
    public String toString() {
        return "[" + getFlightFrom() + "-" + getFlightTo() + "] " +
                new SimpleDateFormat("dd.MM.yyyy HH:mm").format(startTime) + " " +
                getFlightNumber() + " " + getAirplaneType();
    }


    /**
     * Nun folgen die getter und setter Methoden, die die globalen Variablen des Objektes befüllen sollen.
     * Durch die Verwendung von getter und setter Methoden, kann der Zugriff von anderen Klassen besser
     * kontrolliert werden und damit die Anwendung in einem stabilen Zustand bleiben.
     *
     * @return
     */
    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public boolean isLanding() {
        return landing;
    }

    /**
     * In der Datenbank wird dieser Wert als Integer gespeichert. Hier soll umgewandelt werden,
     * sodass eine 1 als true und alles andere als false interpretiert wird.
     */
    public void setLanding(int landing) {
        if (landing == 1) {
            this.landing = true;
        } else {
            this.landing = false;
        }
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getRunnway() {
        return runnway;
    }

    public void setRunnway(int runnway) {
        this.runnway = runnway;
    }

    public String getFlightFrom() {
        return flightFrom;
    }

    public void setFlightFrom(String flightFrom) {
        this.flightFrom = flightFrom;
    }

    public String getFlightTo() {
        return flightTo;
    }

    public void setFlightTo(String flightTo) {
        this.flightTo = flightTo;
    }

    public void setMovingDirection(int movingDirection) {
        this.movingDirection = movingDirection;
    }

    public int getMovingDirection() {
        return movingDirection;
    }
}

