package fmg.ausbildungsstart.projekt.gui;

import fmg.ausbildungsstart.projekt.logic.AirplaneManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ControlDialog extends JFrame {
    private ArrayList<String> history=new ArrayList<>();  //Speichert die zuletzt abgeflogenen/gelandeten Flüge
    private JList historyList =new JList();  //Ein Steuerelement auf dem Fenster zum Anzeigen der Liste

    /**
     * Erstelle das Fenster mit den JButtons Start("►"),Pause("||") und Stop("■") und mit der JList,die die Historie anzeigen wird
     *
     * Die 3 Buttons sollen sich nebeneinander in einem JPanel befinden, welches im oberen Teil des Fensters sein soll (Siehe Screenshot) Hinweis: GridLayout
     * Wenn man einen Button klickt soll die Methode die sein Name anmuten lässt, im AirplaneManager (parameter) aufgerufen werden (play-/pause-/stopApplication)
     *
     * Im unteren Teil des Fensters soll sich einen JList befinden
     *
     * Die größe des Fenster soll so klein wie möglich sein, und vom Layout selbst verwaltet werden
     */
    public ControlDialog(AirplaneManager manager){
        JButton play=new JButton("►");
        play.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                manager.startApplication();
            }
        });

        JButton pause=new JButton("||");
        pause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                manager.pauseApplication();
            }
        });

        JButton stop=new JButton("■");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                manager.stopApplication();
            }
        });

        JPanel buttons=new JPanel();
        buttons.setLayout(new GridLayout(1,3));
        buttons.add(play);buttons.add(pause);buttons.add(stop);

        setLayout(new BorderLayout());
        add(buttons, BorderLayout.NORTH);
        add(historyList, BorderLayout.SOUTH);

        setVisible(true);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
    }


    /**
     * Fügt den Parameter zur ArrayList der Historie hinzu
     * Beinhaltet die Liste mehr als 10 Elemente soll das zuerst hinzugefügte wieder entfernt werden
     * Die JListe von oben muss aktualisiert werden, da sich die Daten geändert haben
     * Zum Schluss soll das fenster wieder auf die minimale Größe geschrumpft bzw. angeordnet werden(genau wie im Konstruktor)
     *
     */
    public void addToHistory(String fly) {
        history.add(fly);
        if (history.size() > 10) {
            history.remove(0);
        }
        historyList.setListData(history.toArray());
        pack();
    }

    /**
     * Löscht die gesamte Historie
     */
    public void reset() {
        history.clear();
    }

    /**
     * Gibt die Historie zurück
     */
    public ArrayList<String> getHistory(){
        return history;
    }
}