package fmg.ausbildungsstart.projekt.gui;

import fmg.ausbildungsstart.projekt.logic.Airplane;
import fmg.ausbildungsstart.projekt.logic.AirplaneManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class GUI extends JFrame {
    private AirplaneManager manager;

    //Eine Liste von allen Flugzeugen die angezeigt wurden, bzw angezeigt werden
    ArrayList<Airplane> airplanes = new ArrayList<>();

    //Speichert die Startkoordination auf denen die Flugzeuge angezeigt werden
    private static Rectangle[] runnwayLocation = new Rectangle[2];


    //Variablen, die die Bilder für das Flugzeug und das Hintergrundbild Cachen.
    private Image airplaneImage;
    private Image airplaneImageinverted;
    private Image backgroundImage;

    //Fenster das die Steuerelemente Start,Pause,Stop beinhaltet
    private ControlDialog controls;

    //Bild, das auf der GUI angezeigt wird
    private Image backbuffer = new BufferedImage(1100, 700, BufferedImage.TYPE_INT_RGB);
    private Graphics display;


    //Konstruktor der in diesem Fall das Fenster erstellt und dessen Eigenschaften festlegt
    public GUI(AirplaneManager manager) {
        //Umspeichern des AirplaneManagers in die globale Variable
        this.manager = manager;

        //Initialisierung aller vorgegebenen Komponenten
        init();

        //Setze die Position dieses Fensters auf x:200;y:0 und als Größe Breite:1100;Höhe:700
        setBounds(200, 0, 1100, 700);

        //Setze als Titel "Azubi Einführungsprojekt"
        setTitle("Azubi Einführungsprojekt");

        //Mach das Fenster Sichtbar
        setVisible(true);
    }

    /**
     * Zuerst soll das Bild das später angezeigt wird initialisiert werden. Dabei soll die
     * Schriftart, -farbe, -größe definiert werden in der die Uhrzeit angezeigt wird.
     * Anschließend sollen die Bilder (Flugzeug, Hintergrund) eingelesen werden und die
     * Positionen der Start/Landebahn definiert werden. Das Fenster soll sich bei einem
     * Klick auf das rote X schließen. Es soll noch der ControlDialog angelegt werden.
     */
    public void init() {
        display = backbuffer.getGraphics();
        display.setFont(new Font("Arial", Font.BOLD, 16));
        display.setColor(Color.white);

        try {
            airplaneImage = ImageIO.read(new File("img/airplane.png"));
            airplaneImageinverted = ImageIO.read(new File("img/airplaneInverted.png"));

            backgroundImage = ImageIO.read(new File("img/hintergrundbild.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        runnwayLocation[0] = new Rectangle(50, 70, 1100, 75);
        runnwayLocation[1] = new Rectangle(50, 720 - 160, 1100, 75);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        controls = new ControlDialog(manager);
    }


    //Zeichnet jedes Flugzeug an die richtige Stelle im Fenster
    private void updateAirplanes(long currentMiliseconds) {
        //Wähle eine geeignete Schleife um alle Flugzeuge in der Liste benutzen zu können
        //Zeichne jedes Flugzeug an die Stelle an der es sich aktuell befindet
        //Tipps:
        //display.drawImage(...)  und  Methoden von Airplane untersuchen!
        //Je nachdem ob die MovingDirection des Flugzeuges 1 oder 0 ist, muss airplaneImage oder airplaneImageinverted gezeichnet werden

        for (Airplane airplane : airplanes) {
            if (airplane.getMovingDirection() == 1) {
                display.drawImage(airplaneImage, airplane.getXPosition(currentMiliseconds), runnwayLocation[airplane.getRunnway() - 1].y, 75, 69, null);
            } else {
                display.drawImage(airplaneImageinverted, airplane.getXPosition(currentMiliseconds), runnwayLocation[airplane.getRunnway() - 1].y, 75, 69, null);
            }
        }
        
    }

    //Fügt ein Flugzeug zur Liste der zu zeichnenden Flugzeuge hinzu
    public void addAirplane(Airplane airplane) {
        //Füge das als Parameter übergebene Flugzeug zur Flugzeugliste hinzu
        airplanes.add(airplane);
        //Füge den Text des Flugzeuges zur History des ControlDialogs hinzu.
        controls.addToHistory(airplane.toString());
    }

    //Zeichnet den gesamten Fensterinhalt
    public void draw(long currentMiliseconds) {
        //Zeichne das Hintergrundbild mithilfe von Graphics
        //Zeichne die Zeit in die obere linke Ecke mithilfe von Graphics
        //Update alle Flugzeuge
        display.drawImage(backgroundImage, 0, 0, 1100, 700, null);
        display.drawString(manager.getFormattedTime(), 10, 45);
        updateAirplanes(currentMiliseconds);

        //Gegeben
        repaint();
    }

    //Von uns gegebene Methoden
    @Override
    public void paint(Graphics g) {
        g.drawImage(backbuffer, 0, 0, null);
    }

    public void reset() {
        controls.reset();
        airplanes = new ArrayList<>();
    }
}