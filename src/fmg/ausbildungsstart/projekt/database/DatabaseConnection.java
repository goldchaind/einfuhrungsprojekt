package fmg.ausbildungsstart.projekt.database;

import fmg.ausbildungsstart.projekt.logic.Airplane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseConnection {

    private String host;
    private int port;
    private String db;
    private String username;
    private String password;


    /**
     * Der Konstruktor soll die übergebenen Variablen (parameter) in die entsprechenden globalen
     * Variablen umspeichern.
     */
    public DatabaseConnection(String host, int port, String database, String username, String password) {
        this.host = host;
        this.port = port;
        this.db = database;
        this.username = username;
        this.password = password;
    }

    /**
     * Aufbauen der Verbindung zur Datenbank. Es soll eine Connection erstellt werden, die
     * die Url "jdbc:oracle:thin:@localhost:1521/fmg_test" aufrufen und dabei den Benutzernamen
     * und das Passwort mitgeben. Verwendet dafür die globalen Variablen.
     */
    public Connection getConnection() throws Exception {
        Connection connection = null;
        connection = DriverManager.getConnection("jdbc:oracle:thin:@" + host + ":" + port + "/" + db, username, password);

        System.out.println("Connected to database");
        return connection;
    }

    /**
     * Gibt alle Flüge die in der Stunde des angefragten Dates sind aus.
     * Die angefragte Zeit soll mit der Methode "getDateWithoutMinutesAndSeconds"
     * in eine passende Zeit umgewandelt werden.
     * Alle Spalten sollen abgefragt werden. Jedoch sollen nur die Flugzeuge zurückgegeben
     * werden, die in der Stunde der Anfrage starten. Verwendet zum Abfragen ein
     * PreparedStatement in das ihr jeweils das Date einfügt.
     * @param date Datum und Uhrzeit der auszugebenden Stunde
     * @return Liste mit Flugzeugen
     */
    public ArrayList<Airplane> getFlightsByHour(Date date){
        ArrayList<Airplane> resultList = new ArrayList<>();

        try {
            java.sql.Date startDate = new java.sql.Date(getDateWithoutMinutesAndSeconds(date).getTime());
            java.sql.Date endDate = new java.sql.Date(getDateWithoutMinutesAndSeconds(date).getTime()+(60*60*1000));
            
            //Hier den SQL-Befehl einfügen
            PreparedStatement preparedStatement =  getConnection().prepareStatement("select starttime, flightnumber, airplanetype, arrival, runway, flightFrom, flightTo, movingdirection from flugdaten where starttime between ? and ?");
            preparedStatement.setDate(1, startDate);
            preparedStatement.setDate(2, endDate);
            ResultSet result = preparedStatement.executeQuery();

            //Für jedes Ergebnis ein Flugzeug erstellen
            while (result.next()) {
                Airplane currAirplane = new Airplane();
                currAirplane.setRunnway(result.getInt("runway"));
                currAirplane.setLanding(result.getInt("arrival"));
                currAirplane.setFlightNumber(result.getString("flightnumber"));
                currAirplane.setStartTime(result.getTimestamp("starttime"));
                currAirplane.setAirplaneType(result.getString("airplanetype"));
                currAirplane.setFlightFrom(result.getString("flightFrom"));
                currAirplane.setFlightTo(result.getString("flightTo"));
                currAirplane.setMovingDirection(result.getInt("movingdirection"));
                resultList.add(currAirplane);
                System.out.println(currAirplane);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultList;
    }

    /**
     * Ähnlich wie die oben genannte Methode sollen hier die Flugzeuge für die nächste Stunde ausgegeben werden.
     * In der Entwicklung versucht man möglichst wenig Code doppelt zu schreiben. Deswegen muss eine Möglichkeit
     * gefunden werden, wie die oben genannte Methode verwendet werden kann.
     * @param date Datum und Uhrzeit der Ausgangszeit. Daten der folgenden Stunde sollen ausgegeben werden.
     * @return Liste mit Flugdaten
     */
    public ArrayList<Airplane> getFlightsNextHour(Date date){
        Date targetDate = new Date();
        targetDate.setTime(date.getTime() + 1000*60*60);
        return getFlightsByHour(targetDate);
    }

    /**
     * Ziel dieser Methode ist es, die Minuten und Sekunden eines beliebigen Dates auf 0 zu setzen. Da die Methoden
     * getMinute() und getSekunde() verwaltet sind soll ein alternativer Ansatz verwendet werden.
     * Das übergebene Date soll mittels SimpleDateFormat in einen String gewandelt werden der nur aus Datum und
     * akuteller Stunde besteht (dd.MM.yyyy HH). Der erstellte String soll ebenfalls mit dem SimpleDateFormat
     * in den Datentyp Date umgewandelt werden. Sollte bei der Verarbeitung ein Fehler auftreten soll null zurück
     * geliefert werden.
     */
    public Date getDateWithoutMinutesAndSeconds(Date dateToConvert){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyy HH");
        try{
            return simpleDateFormat.parse(simpleDateFormat.format(dateToConvert));
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
